# coding:utf-8

"""
setup.py

Setup Script to install all required packages for Debian based OS

Joao Orvalho
"""

import subprocess

required_modules=[
    "matplotlib==1.5.3",
    "numpy==1.14.1",
    "reportlab==3.4.0",
    "python-geoip==1.2",
    "python-geoip-geolite2==2015.303",
    "scapy==2.3.3",
    "impacket==0.9.15",
    "pypdf2==1.26.0"
]

required_packages=[
    "python-pip",
    "python-tk",
    "net-tools",
    "python-pcapy",
    "python-matplotlib",
    "python-mpltoolkits.basemap"
]

def run_command(command):
    '''
    Invoke command as a new system process and return its output.
    '''
    return subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).stdout.read()

if __name__ == '__main__':
    """
    Run Script
    """
    print "Installing Required packages and modules..."

    for package in required_packages:
        run_command("apt-get -y install "+package)

    for module in required_modules:
        run_command("pip install "+module)

    print "--- Finished ---"

