## **Trabalho Linguagens de Programação Dinâmicas**

## Steps to do on Linux

#### Install python 2.7 and pip for it  ####

    #sudo apt update
    #sudo apt dist-upgrade
    #sudo apt install python2.7 python-pip


#### Install Required Packages ####

    apt-get install python-matplotlib (Basemap)
    apt-get install python-mpltoolkits.basemap (Basemap)
    apt-get install net-tools (netstat)
    apt-get install python-tk (matplotlib requirement)
    apt-get install python-pcapy (Sniffer)
    pip install matplotlib==1.5.3
    pip install numpy==1.14.1
    pip install reportlab==3.4.0
    pip install python-geoip==1.2
    pip install python-geoip-geolite2==2015.303
    pip install scapy==2.3.3
    pip install pypdf2==1.26.0
    pip install impacket==0.9.15

#### Useful Commands ####
List all packages installed:

    pip list

Show python Version:

    python -V

----------


**Developed by:** João Orvalho