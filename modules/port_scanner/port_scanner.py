# coding:utf-8

"""
port_scanner.py

Port Scanner

Joao Orvalho
"""

import socket
from datetime import datetime

def run():
    """
    Run Port Scanner module
    """
    try:
        target = raw_input("Enter the remote host IP to scan: ")
        start_port = int(raw_input("Enter the start port number: "))
        end_port = int(raw_input("Enter the last port number: "))

        if (start_port > end_port):
            print "Start port cannot be less than End port "
            return

        print "*" * 40
        print "\n Port Scanner is working on ", target
        print "*" * 40

        time_start = datetime.now()
        for port in range(start_port, (end_port + 1)):
            # AF_INET refers to the address family ipv4, SOCK_STREAM means connection oriented TCP protocol
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socket.setdefaulttimeout(1)
            # Try to connect to a remote socket at address
            result = sock.connect_ex((target, port))
            if result == 0:
                print "Port Open: \t", port
                # print desc[port]
            sock.close()
    except KeyboardInterrupt:
        print "\tStopped Script"
        # sys.exit()
    except socket.gaierror:
        print "Hostname could not be resolved"
        # sys.exit()
    except socket.error:
        print "could not connect to server"
        # sys.exit()

    time_end = datetime.now()
    total = time_end - time_start
    print "Scanning complete in ", total
