# coding:utf-8

"""
read_logs.py

Read Logs

Joao Orvalho
"""

import ipaddress

from urllib import urlopen
import json
import threading
from modules.sqlite_manager import sqlite_manager as db_manager
from geoip import geolite2

auth_log_file="sources/auth.log"
utf_log_file="sources/ufw.log"


#Requires Internet and it's compatible with ipv4 and ipv6
geoip_lookup_url = 'http://freegeoip.net/json/%s'
default_services = {'80': 'HTTP', '443': 'HTTPS', '21': 'FTP', '22': 'SSH', '23': 'Telnet', '53': 'DNS', '53': 'DNS', '53': 'DNS'}
array_misappropriate_auth_attempts = []
array_ufw_blocked_connections=[]

def geo_ip_lookup_geolite2(ip):
    """
    Check geo ip info

    Parameters
    ----------
    ip : string
        ip to check

    Returns
    -------
    list
        ip info from database

    """

    try:
        match = geolite2.lookup(ip)
        if (match== None):
            data = [ip, "", "", "", "", "", ""]
        else:
            country_code = match.country or ""
            country_name=""
            city=""
            region_code=""
            longitude=  match.location[1] if (match.location!=None)  else ""
            latitude= match.location[0] if (match.location!=None)  else ""

            data = [ip,country_code, country_name, city, region_code, str(longitude), str(latitude)]

        return db_manager.store_data([data], db_manager.db_path, db_manager.table_geo_ip)[0]
    except Exception as e:
        raise e


def geo_ip_lookup_api(ip):
    """
    Check ip info in database, if no records:
    Look up the geo information based on the IP address passed in and save IP info in the database

    Parameters
    ----------
    ip : string
        ip to check

    Returns
    -------
    list
        ip info from database
    """
    query = "SELECT * FROM " + db_manager.table_geo_ip + "  WHERE ip='" + ip + "'"
    query_result= db_manager.filter_table(db_manager.db_path, query)
    if(len(query_result) ==0): # Save ip info in database

        try:
            lookup_url = geoip_lookup_url % ip
            json_response = json.loads(urlopen(lookup_url).read())

            data= [ip,json_response['country_code'],json_response['country_name'],json_response['city'],json_response['region_code'],
                str(json_response['longitude']),str(json_response['latitude'])]

            return db_manager.store_data([data], db_manager.db_path, db_manager.table_geo_ip)[0]
        # Catch the exception
        except Exception as e:
            return geo_ip_lookup_geolite2(ip) #if geo_ip_lookup_api function check with other model

    else:
        return query_result[0] # Return ip info from database


def validate_ip(ip):
    """
    Validate ipv4 or ipv6

    Parameters
    ----------
    ip : string
        ip to check

    Returns
    -------
    bool
        true if valid, otherwise false
    """
    try:
        ipaddress.ip_network(unicode(ip))
        valid = True
    except:
        valid = False
    return True if valid else False

def get_ip(split_line):
    """
     Obtain IP from a log line

     Parameters
     ----------
     split_line : string
          list with strings of log line

     Returns
     -------
     string
         IP detected or "" if an ip it's not detected
     """
    for info in split_line:
        if validate_ip(info):
            return info
    return ""

def get_date(split_line):
    """
    Obtain Date from a log line

    Parameters
    ----------
    split_line : string
        log line

    Returns
    -------
    string
        day and month
    """
    return split_line[1] + ' ' + split_line[0]

def get_service(split_line):
    """
    Obtain Service from a log line

    Parameters
    ----------
    split_line : string
        list with strings of authentication log line

    Returns
    -------
    string
        service
    """
    service= split_line[-1].replace('\n', '')
    if service == 'ssh2':
        return 'SSH'
    else:
        return service.upper() # get the last element

def get_time(split_line):
    """
    Obtain Date from a log line

    Parameters
    ----------
    split_line : string
        list with strings of log line

    Returns
    -------
    string
        time
    """
    return split_line[2]

def get_field_value(split_line,field_name):
    """
    Obtain field value, format : field_name=field_value

    Parameters
    ----------
    split_line : list
        list with strings of log line
    field_name : string
        field name

    Returns
    -------
    string
        field value
    """
    for info in split_line:
        if field_name in info:
            return info.replace(field_name+"=", '')
    return ''


def get_misappropriate_auth_attempts(file):
    """
    Read file

    Parameters
    ----------
    file : string
        path of the file with authentication logs
    """
    global array_misappropriate_auth_attempts
    array_misappropriate_auth_attempts=[] # reset array
    with open(file, "r") as doc:
        for line in doc:
            if ': Failed password' in line:
                split_line = line.split(' ')
                ip= get_ip(split_line)
                country_code = geo_ip_lookup_api(ip)[1]
                date=get_date(split_line)
                time= get_time(split_line)
                service= get_service(split_line)
                array_misappropriate_auth_attempts.append([ip, country_code, date, time, service])

        # array_info.append("IP="+ ip + " Country="+ country+ " Date="+ date + " Time=" + time + " Service="+service)
        # print array_info
    doc.close()  # Close opened file

def get_ufw_blocked_connections(file):
    """
    Read file

    Parameters
    ----------
    file : string
        path of the file with ufw logs
    """
    global array_ufw_blocked_connections
    array_ufw_blocked_connections = [] # Reset array
    with open(file, "r") as doc:

        for line in doc:
            if ('UFW BLOCK' in line) and ('DPT'in line):
                split_line = line.split(' ')
                ip = get_field_value(split_line,'SRC')
                country_code = geo_ip_lookup_api(ip)[1]
                date = get_date(split_line)
                time = get_time(split_line)
                service = default_services[get_field_value(split_line,'DPT')]
                array_ufw_blocked_connections.append([ip,country_code,date,time,service])
                #array_info.append("IP=" + ip + " Country=" + country + " Date=" + date + " Time=" + time + " Service=" + service)
                #print array_info
    doc.close() # Close opened file


def get_all_blocked_invalid_accesses():
    """
    Obtain all invalid accesses that were blocked

    Returns
    -------
    list
        Nested list with all invalid accesses that were blocked

    """

    thread1 = threading.Thread(args=[auth_log_file], target=get_misappropriate_auth_attempts)
    thread1.start()

    thread2 = threading.Thread(args=[utf_log_file], target=get_ufw_blocked_connections)
    thread2.start()

    #Wait for all threads
    main_thread = threading.currentThread()
    for t in threading.enumerate():
        if t is not main_thread:
            t.join()

    return array_misappropriate_auth_attempts+array_ufw_blocked_connections


