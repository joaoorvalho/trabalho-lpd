# coding:utf-8

"""
pdf_generator.py

Module to generate pdf's

Joao Orvalho
"""

import datetime
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch

logo = "modules/pdf_generator/estig.png"

def generate(title,list,file_name,additional_text="",annexes =[]):
    """
    Obtain IP from a log line

    Parameters
    ----------
    title : string
        report title
    list : list
        nested list with info to save in report
    file_name : string
        name of the new report file
    file_name: list
        list with annexes paths
    """

    try:
        body = []

        doc = SimpleDocTemplate(file_name+".pdf", pagesize=letter,
                                rightMargin=72, leftMargin=72,
                                topMargin=72, bottomMargin=18)

        styles = getSampleStyleSheet()
        styles.leading = 24
        #Insert Logo
        im = Image(logo, 2 * inch, inch)
        body.append(im)

        # insert space
        body.append(Spacer(1, 12))  # width, height

        #set title
        title = '<font size=12>'+title+'</font>'
        body.append(Paragraph(title, styles["Title"]))


        #write text
        for line in list:
            line= ' '.join(str(x) for x in line)
            body.append(Paragraph(line, styles["Normal"]))

        # insert space
        body.append(Spacer(1, 12))  # width, height

        body.append(Paragraph(additional_text, styles["Normal"]))

        # insert space
        body.append(Spacer(1, 12))  # width, height

        body.append(Paragraph("Annexes", styles["Heading1"])) if len(annexes)>0 else None

        #Insert Annexes
        for annex in annexes:
            pic = Image(annex)
            pic._restrictSize(5 * inch, 10 * inch)
            body.append(pic)

            # insert space
            body.append(Spacer(1, 12))  # width, height

        # insert space
        body.append(Spacer(1, 12)) # width, height

        # set generated time
        formatted_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        generation_date = '<para alignment="right"><font size=9>Relatório gerado em "' + formatted_time + '" </font></para>'
        body.append(Paragraph(generation_date, styles["Italic"]))

        #build
        doc.build(body)
        print "- Generated PDF report successfully: "+file_name+".pdf"
    except Exception as e:
        print '- Unable to generate PDF report. Error: '+ str(e)




if __name__ == '__main__':

    title="Exemplo de título"
    text=[['ola','123'],['ola2','1234']]
    file_name="report"
    try:
        generate(title,text,file_name)
        print "Generated %s.pdf successfully!" % file_name
    except Exception as e:
        print 'Unable to generate report. Error: '+ str(e)