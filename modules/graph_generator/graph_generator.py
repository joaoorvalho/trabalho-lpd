# coding:utf-8

"""
graph_generator.py

Module to generate graphs

Joao Orvalho
"""

import warnings
warnings.filterwarnings("ignore", module="matplotlib")

import matplotlib
matplotlib.use('agg') # for png
import matplotlib.pyplot as plt


import numpy as np
import hashlib
import os
from datetime import datetime

path_to_save= "modules/graph_generator/img/"

def create_pic_dir():
    """
    Create img dir if it doesn't exist
    """
    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

def auto_label(rects):
    """
    Attach a text label above each bar

    Parameters
    ----------
    rects : Container
        rects list of chart bars to put labels
    """
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2., height, '%d' % int(height), ha='center', va='bottom')

def generate_graphic_bar(labels, stats, subtitles):
    """
    Generate a histogram

    Parameters
    ----------
    labels : list
        list of Strings with labels for the histogram
    stats : list
        list of Integers with statistics
    subtitles : list
        title of graphic,  title of X axis , title of Y axi

    Returns
    -------
    string
        graph path (.png)
    """
    index = np.arange(len(stats))  # the x locations for the groups
    width = 0.6  # the width of the bars
    global ax # create global var
    fig, ax = plt.subplots()
    bar = ax.bar(index + 2, stats, width, color='b')
    ax.set_ylabel(subtitles[1])
    ax.set_xlabel(subtitles[2])
    ax.set_title(subtitles[0])
    ax.set_xticks((index + width / 2) + 2)
    plt.rcParams.update({'font.size': 10})
    ax.set_xticklabels(labels, rotation='vertical')
    auto_label(bar)
    plt.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.35)

    # Generate name for image "print"
    t = str(datetime.now())  # Time (Timestamp)
    t_md5 = hashlib.md5(t).hexdigest()  # Time in MD5
    path = path_to_save+"{0}".format(t_md5)
    create_pic_dir()
    # Save image and removes all of the extra white space around your figure
    plt.savefig(path, bbox_inches='tight')
    plt.clf() # Clear figure
    return path+".png"
    #plt.show()

def generate_pizza_graphic(labels, stats, title):
    """
    This function generates a pie chart / pizza graphic

    Parameters
    ----------
    labels : list
        list of Strings with labels for the histogram
    stats : list
        list of Integers with statistics
    title : string
        title of graphic

    Returns
    -------
    string
        graph path (.png)
    """

    x = np.char.array(labels)
    y = np.array(stats)
    colors = ['yellowgreen', 'red', 'gold', 'lightskyblue', 'white', 'lightcoral', 'blue', 'pink', 'darkgreen',
              'yellow', 'grey', 'violet', 'magenta', 'cyan']
    porcent = 100. * y / y.sum()

    patches, texts = plt.pie(y, colors=colors, startangle=90, radius=1.2)

    labels = ['{0} - {1:1.2f} %'.format(i, j) for i, j in zip(x, porcent)]

    sort_legend = True
    if sort_legend:
        patches, labels, dummy = zip(*sorted(zip(patches, labels, y),
                                             key=lambda x: x[2],
                                             reverse=True))

    plt.legend(patches, labels, bbox_to_anchor=(-0.1, 1.),
               fontsize=8)
    plt.suptitle(title)

    # Generate name for image "print"
    t = str(datetime.now())  # Time
    t_md5 = hashlib.md5(t).hexdigest()  # Time in MD5
    path = path_to_save + "{0}".format(t_md5)

    create_pic_dir()
    plt.savefig(path+'.png', bbox_inches='tight')
    plt.clf() # Clear figure

    return path + ".png"


