# coding:utf-8

"""
db_manager.py

SQLite DataBase Manager

Joao Orvalho
"""

import sqlite3

fields_geo_ip="ip TEXT,country_code TEXT, country_name TEXT, locality TEXT, region TEXT, longitude TEXT, latitude TEXT"
fields_blocked_accesses="ip TEXT,country_code TEXT, date TEXT, time TEXT, service TEXT"
fields_program_lock="username TEXT, password TEXT"
table_geo_ip="geo_ip"
table_blocked_accesses="blocked_accesses"
table_program_lock="program_lock"
db_path= "modules/sqlite_manager/data/mydb.db"

def create_table(db_name,table_name,fields_info,reset=1):
    """
    Create SQL table

    Parameters
    ----------
    db_name : string
        DataBase name
    table_name: string
        Table name
    fields_info  : string
        field names and types separated by commas
    reset: integer
        1 to reset/create table, 0 to create table if not exist
    """
    # Creates or opens a databse
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    try:
        if(reset):
            c.execute('DROP TABLE IF EXISTS '+table_name)
            c.execute('CREATE TABLE '+table_name+' ('+fields_info+')')
        else:
            c.execute('create table if not exists ' + table_name + ' (' + fields_info + ')')

        conn.commit()
    # Catch the exception
    except Exception as e:
        # Roll back any change if something goes wrong
        conn.rollback()
        raise e
    finally:
        conn.close()  # Close the db connection

def store_data(list, db_name,table_name):
    """
    Stores the data in the dataBase

    Parameters
    ----------
    db_name : string
        DataBase name
    list: list
        Nested list with data to store
    table_name: string
        Table name
    Returns
    -------
    list
        nested list stored
    """

    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    value_marks = ('?,' * (len(list[0]) - 1)) +'?'

    try:
        c.executemany('INSERT INTO '+table_name+' VALUES ('+value_marks+')', list)
        conn.commit()
    # Catch the exception
    except Exception as e:
        # Roll back any change if something goes wrong
        conn.rollback()
        raise e
    finally:
        conn.close()  # Close the db connection


    return list

def print_table(db_name,table_name):
    """
    Display SQL data

    Parameters
    ----------
    db_name : string
        DataBase name
    table_name: string
        Table name
    """
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    try:
        c.execute("SELECT * FROM "+table_name)
        dataList = c.fetchall()  # read teses Table to List

        nrows = len(dataList)  # number of rows
        ncols = len(dataList[0])  # number of cols

        for rowx in range(nrows):
            for colx in range(ncols):
                value = dataList[rowx][colx]
                print value,
            print
    # Catch the exception
    except Exception as e:
        # Roll back any change if something goes wrong
        conn.rollback()
        raise e
    finally:
        conn.close()  # Close the db connection

def retrieve_data(db_name,table_name):
    """
    Retrieve SQL data

    Parameters
    ----------
    db_name : string
        DataBase name
    table_name: string
        Table name
    Returns
    -------
    list
        nested list with table data
    """
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    data=[]
    try:
        c.execute("SELECT * FROM "+table_name)
        data = c.fetchall()  # read teses Table to List
    # Catch the exception
    except Exception as e:
        # Roll back any change if something goes wrong
        conn.rollback()
        raise e
    finally:
        conn.close()  # Close the db connection

    return data




def edit_table(dbName,query):
    """
    Edit table data

    Parameters
    ----------
    dbName: string
        DataBase name
    query: string
        Query to handle
    """

    conn = sqlite3.connect(dbName)
    c = conn.cursor()
    try:
        c.execute(query)
        conn.commit()
    # Catch the exception
    except Exception as e:  # Roll back any change if something goes wrong
        conn.rollback()
        raise e

    finally:
        conn.close()  # Close the db connection


def filter_table(dbName,query):
    """
    Filter table data

    Parameters
    ----------
    dbName: string
        DataBase name
    query: string
        Query to handle

    Returns
    -------
    list
        nested Query with the result
    """

    conn = sqlite3.connect(dbName)
    c = conn.cursor()
    try:
        c.execute(query)
        data_list = c.fetchall()

    # Catch the exception
    except Exception as e:  # Roll back any change if something goes wrong
        conn.rollback()
        raise e

    finally:
        conn.close()  # Close the db connection

    return data_list

def get_rows_and_cols(list):
    """
    Get Rows and Cols values

    Parameters
    ----------
    list: list
        Nested list with (col,row) values

    Returns
    -------
    list
        Nested list with [cols values,rows values]
    """
    row,col=[],[]
    for value in list:
        row.append(value[1])
        col.append(value[0])

    return [col,row]


def db_filter_invalid_accesses_by_country_code(blocked_invalid_accesses):
    """
    Get the number of blocked_accesses by each country code

    Parameters
    ----------
    blocked_invalid_accesses: list
        Nested list with all blocked invalid accesses

    Returns
    -------
    list
        Nested list with number of blocked_accesses by each country
    """
    query = "SELECT country_code,count(country_code) FROM blocked_accesses GROUP BY country_code"
    list = filter_table(db_path, query)
    return list

def db_filter_invalid_accesses_by_services():
    """
    Get the number of blocked_accesses by each service

    Parameters
    ----------
    blocked_invalid_accesses: list
        Nested list with all blocked invalid accesses

    Returns
    -------
    list
        Nested list with number of blocked_accesses by each service
    """
    query = "SELECT service,count(service) FROM blocked_accesses GROUP BY service"
    list = filter_table(db_path, query)
    return list

