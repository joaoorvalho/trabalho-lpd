# coding:utf-8

"""
passive_sniffer.py

Port Scanner

Joao Orvalho
"""

import socket
import struct
import binascii
# The 0x0800 code defines the protocol ETH_P_IP
s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0800))

def get_tcp_flag(binary_str):
    """
    Given a binary number, return associated TCP flag

    Parameters
    ----------
    binary_str: string
        Binary value

    Returns
    -------
    string
        List of active TCP flags separated by commas
    """
    binary_str= binary_str[::-1] # Reverse a string
    flags = {
        0 : "Fin",
        1 : "Syn",
        2 : "Reset",
        3 : "Push",
        4 : "Acknowledgement",
        5 : "Urgent",
        6 : "ECN - Echo",
        7 : "Congestion Window Reduced(CWR)",
        8 : "Nonce",
        9 : "Reserved"
    }
    activated_flags=[]
    cont=0
    for char in binary_str:
        if char=='1':
            activated_flags.append(flags[int(cont)])
        cont=cont+1
    return ','.join(activated_flags)

def dec_to_bin(num):
    """
    Convert decimal value to binary

    Parameters
    ----------
    binary_str: string
        Binary value

    Returns
    -------
    string
        List of active TCP flags separated by commas
    """
    return '{0:010b}'.format(num)

def main():
    """
    Main function
    """
    print "\n Sniffing... \n "
    while True:
        # Create a buffer of 2048
        pkt = s.recvfrom(2048) # Incoming frames
        # take the first 14 bytes from the pkt

        ethhead = pkt[0][0:14] # Ethernet frame is 14 bytes long
        # ! shows network bytes, and 6s shows 6 bytes
        eth = struct.unpack("!6s6s2s",ethhead)
        print "--------Ethernet Frame--------"
        # Hexadecimal representation of the binary data
        print "Destination mac",binascii.hexlify(eth[0])
        print "Source mac",binascii.hexlify(eth[1])
        binascii.hexlify(eth[2])

        # Extract the next 20 bytes of data (IP header)
        ipheader = pkt[0][14:34]
        ip_hdr = struct.unpack("!12s4s4s",ipheader)
        print "-----------IP------------------"
        # converts a 32-bit packed IPv4 address (a string that is four characters in length)
        # to its standard dotted-quad string representation
        print "Source IP", socket.inet_ntoa(ip_hdr[1])
        print "Destination IP", socket.inet_ntoa(ip_hdr[2])

        print "---------TCP----------"
        # Extracts the next 20 bytes of data
        tcpheader = pkt[0][34:54]
        #tcp_hdr = struct.unpack("!HH16s",tcpheader)
        tcp_hdr = struct.unpack("!HH9ss6s",tcpheader) # divided into 3 parts
        print "Source Port ", tcp_hdr[0]
        print "Destination port ", tcp_hdr[1]
        #Return the hexadecimal representation of the binary data
        try:
            decimal_flag= eval("0x" + binascii.hexlify(tcp_hdr[3])) # convert hexadecimal to decimal
            print "Flag " + binascii.hexlify(tcp_hdr[3]) + " ("+get_tcp_flag(dec_to_bin(decimal_flag))+")"
        except:
            pass


def run():
    """
    Run Program
    """
    try:
        main()
    except KeyboardInterrupt:
        print "\tStopped Script"
    except Exception:
        print "Error occurred!"


