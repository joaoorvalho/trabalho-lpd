# coding:utf-8

"""
program_lock.py

Module to lock program with password

Joao Orvalho
"""

import hashlib
from modules.sqlite_manager import sqlite_manager as db_manager


def create_default_login():
    """
    Create default login
    """
    default_login= ['admin',""]
    db_manager.store_data([default_login], db_manager.db_path, db_manager.table_program_lock)


def read_password():
    """
    Read stored password from database
    """
    if len(db_manager.retrieve_data(db_manager.db_path,db_manager.table_program_lock)) == 0:
        create_default_login()

    query = "SELECT password FROM " + db_manager.table_program_lock + " WHERE username='admin' LIMIT 1"
    query_result = db_manager.filter_table(db_manager.db_path, query)
    return query_result[0][0]

def login(password):
    """
    Login Function

    Parameters
    ----------
    password: string
        Typed password

    Returns
    -------
    bool
        True if correct password, otherwise False
    """
    hashed_password = hashlib.sha512(password).hexdigest() if password!="" else ""
    return True if hashed_password == read_password() else False

def change_password(password):
    """
    Change current password

    Parameters
    ----------
    password: string
        New password
    """

    if password!="":
        hashed_password = hashlib.sha512(password).hexdigest()
        query = "UPDATE "+db_manager.table_program_lock+" SET password ='"+hashed_password+"' WHERE username = 'admin'"
        db_manager.edit_table(db_manager.db_path, query)
    else:
        query = "UPDATE "+db_manager.table_program_lock+" SET password ='' WHERE username = 'admin'"
        db_manager.edit_table(db_manager.db_path, query)




