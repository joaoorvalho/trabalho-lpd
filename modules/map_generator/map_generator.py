# coding:utf-8

"""
map_generator.py

Read Logs

Joao Orvalho

"""
try:
    from mpl_toolkits.basemap import Basemap
except:
    print "Failed importing Basemap module, make sure it's installed in Python 2.7 folder"

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
import matplotlib.pyplot as plt
import numpy as np
import os

import hashlib
from datetime import datetime

path_to_save= "modules/map_generator/img/"

def create_pic_dir():
    """
    Create img dir if it doesn't exist
    """
    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)


def genenerate(lons,lats):
    """
    Generate Map

    Parameters
    ----------
    lons : list
        list of longitude coordinates
    lats : list
        list of latitude coordinates

    Returns
    -------
    string
        graph path (.png)
    """
    eq_map = Basemap(projection='robin', resolution='l', area_thresh=1000.0,
                     lat_0=0, lon_0=-130)
    eq_map.drawcoastlines()
    eq_map.drawcountries()
    eq_map.fillcontinents(color='gray')
    eq_map.drawmapboundary()
    eq_map.drawmeridians(np.arange(0, 360, 30))
    eq_map.drawparallels(np.arange(-90, 90, 30))

    x, y = eq_map([float(i) for i in lons], [float(i) for i in lats]) # Convert to float and set values
    eq_map.plot(x, y, 'ro', markersize=6)

    title_string = u"Mapa Mundo com a geolocalização dos acessos bloqueados\n"

    plt.title(title_string)

    # Generate name for image "print"
    t = str(datetime.now())  # Time
    t_md5 = hashlib.md5(t).hexdigest()  # Time in MD5
    path = path_to_save+"{0}".format(t_md5)
    create_pic_dir()
    plt.savefig(path + '.png', bbox_inches='tight')
    plt.clf()  # Clear figure

    return path+".png"