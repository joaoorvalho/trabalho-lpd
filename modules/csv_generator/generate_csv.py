# coding:utf-8

"""
generate_csv.py

Read Logs

Joao Orvalho
"""

import csv

def generate(file_name,data,column_title_row):
    """
    Obtain IP from a log line

    Parameters
    ----------
    file_name : string
        csv file name
    data : list
        nested list with data to save in the file
    column_title_row : list
        columns titles
    """
    try:
        out = csv.writer(open(file_name+".csv","wb"), delimiter=',',quoting=csv.QUOTE_ALL)
        out.writerow(column_title_row)
        out.writerow([]) #blank line
        for value in data:
            out.writerow(value)
        print "- Generated CSV report successfully: " + file_name + ".csv"
    except Exception as e:
        print '- Unable to generate CSV report. Error: ' + str(e)


