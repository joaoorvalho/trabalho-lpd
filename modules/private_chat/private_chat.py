# coding:utf-8

"""
private_chat.py

Private chat

Joao Orvalho
"""

import socket
from Crypto.PublicKey import RSA
from Crypto import Random
random_generator = Random.new().read

#Generate 1024 bits keys
key = RSA.generate(1024, random_generator)

#Extract private Key
my_private_key = key.exportKey()

# External public key (other machine)
public_external_key=""

#Extract public Key
my_public_key = key.publickey().exportKey()


def client(s):
    """
    Function to connect o Server

    Parameters
    ----------
    s : object
        socket
    """
    global public_external_key
    host = raw_input('Server ip (to connect): ') # host ip
    port = 12345

    s.connect((host, port))
    print 'Connected to', host

    # Change public Key with Server
    exter_key= s.recv(1024)  # receive public external key
    s.send(my_public_key) # Send public Key
    public_external_key= RSA.importKey(exter_key) # Convert str to proper format

    while True:
        msg = raw_input("Enter something for the server: ")
        encrypted_msg = public_external_key.encrypt(msg, None)  # The second parameter is useless
        s.send(str(encrypted_msg))
        # Halts
        print '[Waiting for response...]'
        msg = s.recv(1024)
        print key.decrypt(eval(msg))  # interprets a string as code.




def server(s):
    """
    Function to connect o Client

    Parameters
    ----------
    s : object
       socket
    """
    global public_external_key
    host = raw_input('Your Listening IP: ') # ex: server private ip

    port = 12345 #Port of Server
    s.bind((host, port))

    s.listen(1) # number of comunications at the same time
    c = None

    while True:
        if c is None:
            # Halts
            print '[Waiting for connection...]'
            c, addr = s.accept()
            print 'Got connection from', addr

            # Change public Key with Client
            c.send(my_public_key) # Send my public key
            exter_key = c.recv(1024)  # receive public external key
            public_external_key = RSA.importKey(exter_key)  # Convert str to proper format

        else:
            # Halts
            print '[Waiting for response...]'
            msg=c.recv(1024)
            print key.decrypt(eval(msg)) # interprets a string as code.

            msg = raw_input("Enter something to the client: ")
            encrypted_msg = public_external_key.encrypt(msg, None)  # The second parameter is useless
            c.send(str(encrypted_msg))


def run():
    """
    Start app
    """
    print "\nAvailable options:"
    print "[0] Become Server"
    print "[1] Become Client"

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        option = raw_input("\nPlease select a valid option [0-1]: ")

        if option == '0':
            server(s)
        elif option == '1':
            client(s)
        else:
            print "Invalid option! Try again...\n"
            run()
    except KeyboardInterrupt:
        print "\tStopped Script"
    except Exception as e:
        print "Error! Message: "+ str(e)
        run() # Restart app
    finally:
        s.close()


