# coding:utf-8

"""
program.py

Trabalho LPD

Joao Orvalho
"""

from modules.read_logs import read_logs
from modules.sqlite_manager import sqlite_manager as db_manager
from modules.csv_generator import generate_csv
from modules.pdf_generator import pdf_generator
from modules.graph_generator import graph_generator
from modules.udp_flooding import udp_flooding as udp_flood
from modules.port_scanner import port_scanner as port_scan
from modules.active_connections import active_connections as active_conn
from modules.syn_flooding import syn_flooding as syn_flood
from modules.map_generator import map_generator as map_gen
from modules.encrypt_pdf import encrypt_pdf
from modules.program_lock import program_lock
from modules.sniff_pass import sniff_pass as sniff
from modules.private_chat import private_chat as priv_chat
from modules.passive_sniffer import passive_sniffer as passive_sniff
import os
import datetime
import getpass

def change_password():
    """
    Call change password Module
    """
    try:
        password = getpass.getpass("Enter the new login password: ")
        program_lock.change_password(password)
        print "Password changed successfully!"
    except KeyboardInterrupt:
        print "\tStopped Script"
    except:
        print "We're sorry. We were not able to change your password"


def login(password):
    """
    Login Function

    Parameters
    ----------
    password: string
        Typed password

    Returns
    -------
    bool
        True if correct password, otherwise False

    """
    return program_lock.login(password)


def passive_sniffer():
    """
    Call passive sniffer module
    """
    passive_sniff.run()

def private_chat():
    """
    Call private chat module
    """
    priv_chat.run()

def sniff_pass():
    """
    Call sniff pass password module
    """
    sniff.run()


def udp_flooding():
    """
    Call udp flooding module
    """
    udp_flood.run()


def port_scanner():
    """
    Call port scanner module
    """
    port_scan.run()

def syn_flooding():
    """
    Call Syn Flood module
    """
    syn_flood.run()

def check_active_connections():
    """
    Call active connections module
    """
    active_conn.run()


def create_report_folder(subject):
    """
    Create folder to report

    Parameters
    ----------
    subject : string
        Report Subject

    Returns
    -------
    string
        folder path of the created folder
    """
    formatted_time = datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S')
    folder_name = subject + "_report_" + formatted_time
    if not os.path.exists('reports/' + folder_name):
        os.makedirs('reports/' + folder_name)
    return 'reports/' + folder_name


def check_blocked_invalid_accesses():
    """
    Call module to check blocked invalid accesses from logs
    """
    print "Loading..."
    # Create report folder
    report_folder = create_report_folder("BlockedInvalidAccesses")

    # Read Logs
    blocked_invalid_accesses = read_logs.get_all_blocked_invalid_accesses()

    db_manager.store_data(blocked_invalid_accesses, db_manager.db_path, db_manager.table_blocked_accesses)

    # Generate Graphs
    annexes = []

    # Pizza Graph
    graphic_values = db_manager.get_rows_and_cols(db_manager.db_filter_invalid_accesses_by_services())

    title = u'Tentativas indevidas de acesso por serviço'
    annexes.append(graph_generator.generate_pizza_graphic(graphic_values[0], graphic_values[1], title))

    # Bar Graph
    graphic_values = db_manager.get_rows_and_cols(
        db_manager.db_filter_invalid_accesses_by_country_code(blocked_invalid_accesses))
    subtitles = [u'Tentativas indevidas de acesso por país', u'Número de bloqueios', u'Código dos Países']
    annexes.append(graph_generator.generate_graphic_bar(graphic_values[0], graphic_values[1], subtitles))

    # Ask for PDF file Password (set)
    pdf_password = getpass.getpass("Encryption password for PDF report (Blank=No Password): ")

    # Generate CSV
    column_title_row = ("Ip", "Country Code", "Date", "Time", "Service")
    generate_csv.generate(report_folder + '/report', blocked_invalid_accesses, column_title_row)

    #Generate MAP
    try:
        dbName = db_manager.db_path
        query = "SELECT longitude,latitude FROM geo_ip WHERE longitude!='' AND latitude!=''"
        coordinates=db_manager.get_rows_and_cols(db_manager.filter_table(dbName, query))
        annexes.append(map_gen.genenerate(coordinates[0],coordinates[1]))
    except:
        print "We were not able to generate the MAP..."

    # Generate PDF
    title = "Tentativas indevidas de acesso bloqueadas"
    number_blocked_invalid_accesses = len(blocked_invalid_accesses)
    blocked_invalid_accesses = [("<b>Ip |</b>", " <b>Country Code</b> |", " <b>Date</b> |", " <b>Time</b> |",
                                 " <b>Service</b>"), ["<br/><br/>"]] + blocked_invalid_accesses \

    additional_text= u"<b>Número total de tentativas indevidas de acesso bloqueadas: </b> " + str(number_blocked_invalid_accesses)
    pdf_generator.generate(title, blocked_invalid_accesses, report_folder + '/report',additional_text, annexes)


    #Encrypt PDF
    encrypt_pdf.run(report_folder + '/report.pdf',pdf_password)


def create_required_tables():
    """
    Create required tables in DB if not exist
    """
    db_manager.create_table(db_manager.db_path, db_manager.table_program_lock, db_manager.fields_program_lock, 0)
    db_manager.create_table(db_manager.db_path, db_manager.table_geo_ip, db_manager.fields_geo_ip, 0)
    db_manager.create_table(db_manager.db_path, db_manager.table_blocked_accesses, db_manager.fields_blocked_accesses,1)


def menu():
    """
    Program main menu
    """

    # Create report folder if not exist
    if not os.path.exists('reports'):
        os.makedirs('reports')

    while True:
        print ("\n######## Available options: ########")
        print ("[1]- Port Scanner")
        print ("[2]- UDP Flood")
        print ("[3]- SYN Flood")
        print ("[4]- Check active connections")
        print ("[5]- Generate 'blocked invalid accesses' reports")
        print ("[6]- Sniff packages with plain passwords")
        print ("[7]- Private Socket Chat")
        print ("[8]- Passive Sniffer")
        print ("\n[9]- Change program password")
        print ("[0]- exit")

        try:
            option = raw_input("\nOption [0-9]: ")
            if (len(option) == 1):  # only one digit
                option = int(option)
                if ((option >= 0) and (option <= 9)):
                    if (option == 0):
                        break
                    else:
                        choices = {1: port_scanner, 2: udp_flooding, 3: syn_flooding, 4:check_active_connections,
                                   5: check_blocked_invalid_accesses, 6: sniff_pass ,7: private_chat, 8: passive_sniffer, 9:change_password}
                        (choices[option]())  # call chosen function
                else:
                    raise Exception("It's not a valid number")
            else:
                raise Exception("It's not a valid number")  # print with Ansi colors
        except Exception as e:
            print  Exception("\x1B[" + "31m" + "\nInvalid option, error details: \"" + str(
                e) + "\". Please try again... \n" + "\x1B[" + "0m")  # print with Ansi colors


if __name__ == '__main__':
    # Create/Reset Required table and fill with data
    create_required_tables()

    # Show login
    password= getpass.getpass("Enter the login password: ")
    if(login(password)):
        menu()
    else:
        print "Invalid Password..."
